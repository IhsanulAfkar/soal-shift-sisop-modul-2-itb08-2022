#include <stdio.h>
#include <wait.h>
#include <stdlib.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <syslog.h>
#include <string.h>

//fungsi untuk memproses
void process(char run[], char *argv[]){
    int status;
    pid_t child = fork();

    if(child == 0){
        execv(run, argv);
    } else while(wait(&status) > 0);
}

//membuat directory baru
void createDir(char genre[]){

    char path[225];
    snprintf(path, sizeof path, "drakor/%s", genre);
    char *argv[ ] = {"mkdir", "-p", path, NULL};
    
    process("/usr/bin/mkdir", argv);
}

//mengcopy semua data file
void copyAll(char name[], char genre[], char fileName[]) {

    char path[225];
    char newFile[225];
    snprintf(path, sizeof path, "drakor/%s", fileName);
    snprintf(newFile, sizeof newFile, "drakor/%s/%s.png", genre, name);
    
    char *argv[] = {"cp", path, newFile, NULL};
    char run[] = "/usr/bin/cp";
    process(run, argv);
}

//menghapus file yang tidak penting
void deleteFile(char fileName[]){
    char path[225];
    snprintf(path, sizeof path, "drakor/%s", fileName);

    char *argv[ ] = {"rm", path, NULL};
    char run[ ] = "/usr/bin/rm";
    process(run, argv);
}

//memproses semua data dan memisahkan sesuai genre
void processAll(){
    DIR *d = opendir("drakor");
    struct dirent *dir;

    char ls[50][100];
    int i=0;
    

    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, "..") == 0) continue;
            char object[225];
            snprintf(object, sizeof object, "%s", dir->d_name);
            
            char str[225];
            strcpy(str, object);

            char *format = strtok(str, ";");

            char drakor[100];
            snprintf(drakor, sizeof drakor, "%s", format);
            format = strtok(NULL, ";");
           

            char year[50];
            snprintf(year, sizeof year, "%s", format);
            format = strtok(NULL, ";._");

            char genre[50];
            snprintf(genre, sizeof genre, "%s", format);
            format = strtok(NULL, ";");

            createDir(genre);

            copyAll(drakor, genre, object);
            
            snprintf(ls[i], sizeof ls[i], "%s;%s;%s", genre, year, drakor);
            i++;
      
            //memisahkan 2 poster
            if(strlen(format) > 3){
                char drakor2[100];
                snprintf(drakor2, sizeof drakor2, "%s", format);
                format = strtok(NULL, ";");

                char year2[50];
                snprintf(year2, sizeof year2, "%s", format);
                format = strtok(NULL, ";._");
        

                char genre2[50];
                snprintf(genre2, sizeof genre2, "%s", format);
                format = strtok(NULL, "_");
  
                createDir(genre2);
            
                copyAll(drakor2, genre2, object);

                snprintf(ls[i], sizeof ls[i], "%s;%s;%s", genre2, year2, drakor2);
                i++;
       
            }

            deleteFile(object);

        }
        closedir(d);
    }

    //mengurutkan nama dan tahun berdasarkan genre
    int it,jt;
    char temp[105];
    //Buble Sort algorithm
    for(it=0; it<20; it++){
        for(jt=0; jt<20-1-it; jt++){
            if(strcmp(ls[jt], ls[jt+1]) > 0){
                //swap ls[j] and ls[j+1]
                strcpy(temp, ls[jt]);
                strcpy(ls[jt], ls[jt+1]);
                strcpy(ls[jt+1], temp);
            }
        }
    }
    strcpy(temp, "");

    for(it=0; it<20; it++) printf("%i - %s\n", it, ls[it]);

   

    for(it=1; it<20; it++){
        //printf("%s\n", ls[it]);
        char st[2255];
        snprintf(st, sizeof st, "%s", ls[it]);
        //printf("%s\n", st);

        char *format = strtok(st, ";");

        char genre[15];
        snprintf(genre, sizeof genre, "%s", format);
        format = strtok(NULL, ";");

        //printf("get genre %s\n", genre);

        char year[15];
        snprintf(year, sizeof year, "%s", format);
        format = strtok(NULL, ";");

        //printf("get year %s\n", year);

        char drakor[100];
        snprintf(drakor, sizeof drakor, "%s", format);
        format = strtok(NULL, ";");

        //printf("get drakor %s\n", drakor);

        char path[50];
        snprintf(path, sizeof path, "drakor/%s/data.txt", genre);
        

        FILE *data;
        data = fopen(path, "a+");
        
        if(strcmp(temp, genre) != 0){
            
            fprintf(data, "genre: %s\n\n", genre);
        }
        fprintf(data, "nama: %s\n", drakor);
        fprintf(data, "rilis: tahun %s\n\n", year);
       

        fclose(data);
        strcpy(temp, genre);
    }
}

int main(){
    int status;
    pid_t child = fork();

    if(child == 0){
        char *argv[ ] = {"unzip", "drakor.zip", "*.png", "-d", "drakor", NULL};
        execv("/bin/unzip", argv);
    } else{
        while(wait(&status) > 0);
        processAll();

        return(0);
    }
}
