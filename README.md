# soal-shift-sisop-modul-1-ITB08-2022

### <b> Anggota Kelompok: </b>
##### 1. 5027201001 - Najwa Amelia Qorry 'Aina
##### 2. 5027201024 - Muhammad Ihsanul Afkar
##### 3. 5027201072 - Shafira Khaerunnisa Latif
---

## Soal 1
Mas Refadi adalah seorang wibu gemink.Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu. 

## Penyelesaian soal 1
Berikut adalah kode keseluruhan untuk [soal1.c](https://gitlab.com/IhsanulAfkar/soal-shift-sisop-modul-2-itb08-2022/-/blob/main/soal1/soal1.c).

### 1a
Pertama-tama, kita diminta untuk mendownload database [characters](https://drive.google.com/file/d/1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp/view) dan [weapons](https://drive.google.com/file/d/1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT/view). <br>
Berikut adalah kode untuk mendownload dari url yang diberikan. <br>
```c
void download(char *url, char *filename)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"wget", "-q", "--no-check-certificate", url, "-O", filename, NULL};
        execv("/bin/wget", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
Penjelasan kode:
- `pid_t child_id` adalah untuk mengembalikan process ID dari proses saat ini.
- `child_id = fork()` untuk membuat child dari proses yang sedang berjalan. 
- `if(child_id == 0)` adalah jika proses adalah `child`, maka akan masuk `if`.
- `char *argv[]` untuk menampung kata-kata sesuai yang tertulis command line untuk dijadikan parameter pada fungsi `execv()`.
- `execv("/bin/wget, argv")` menjalankan fungsi `wget` dengan parameter yang ada di variable `argv`.
- `else if (child_id > 0)` jika parent, maka masuk else.
- `wait(NULL)` adalah sintaks untuk menunggu hingga child selesai mengeksekusi programnya.

Selanjutnya file-file tersebut akan di unzip sesuai nama dari folder di dalamnya. <br>
Berikut adalah kode untuk meng-unzip dari nama file yang diberikan.
```c
void unzip(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"unzip", "-n", "-q", filename, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
- Untuk penjelasan, kurang lebih sama seperti mendownload program
<br>

Selanjutnya, kita diminta untuk membuat direktori yang bernama `gacha_gacha`.
Berikut adalah kode untuk membuat direktori.
```c
void create_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        DIR *dir = opendir(dirname);
        if (dir)
        {
            closedir(dir);
        }
        else
        {
            char *argv[] = {"mkdir", dirname, NULL};
            execv("/bin/mkdir", argv);
        }
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```
- `DIR *dir = opendir(dirname)` adalah untuk mengecek apakah sudah ada direktori bernama `dirname`, jika ada maka lakukan `closedir()` dan `exit`.
Jika belum ada, maka buat direktori tersebut.
<br>

> Berikut adalah contoh isi dari `weapons/`

![soal1_5.png](images/soal1_5.png)


### 1b, 1c, dan 1d
Terdapat ketentuan gacha seperti berikut:
- Jika counter gacha adalah genap, maka hasilnya adalah `weapons`. Jika ganjil, maka `characters`. 
- Untuk setiap counter gacha kelipatan 10, maka hasil tersebut akan dimasukkan kedalam file dengan format nama `{Hh:Mm:Ss}_gacha_{jumlah-gacha}.txt`. Jarak pembuatan antar file berjarak 1s.
- Untuk setiap counter gacha kelipatan 90, maka hasil tersebut akan dimasukkan kedalam folder dengan format nama `total_gacha_{jumlah-gacha}`.

--- 
> Bagian Revisi


Berikut adalah kode untuk program gacha pada `int main()`.
```c
int total_gacha = 0;
while (1)
{
    // create dir name
    char current_dir[100], formatted_dir[100], num_dir[5];
    strcpy(current_dir, "gacha_gacha/");
    int round_to_90 = total_gacha - (total_gacha % 90);
    sprintf(num_dir, "%d", round_to_90);
    strcat(current_dir, "total_gacha_");
    strcat(current_dir, num_dir);
    strcat(current_dir, "/");
    create_dir(current_dir);

    for (int j = 0; j < 9; j++)
    {
        // create filename
        time_t timer;
        char formatted_time[100], number[5];
        char temp_dir[100];
        strcpy(temp_dir, current_dir);
        struct tm *tm_info;
        timer = time(NULL);
        tm_info = localtime(&timer);
        strftime(formatted_time, 100, "%H:%M:%S", tm_info);
        int round_to_10 = total_gacha - (total_gacha % 10);
        sprintf(number, "%d", round_to_10);

        strcat(temp_dir, formatted_time);
        strcat(temp_dir, "_gacha_");
        strcat(temp_dir, number);
        strcat(temp_dir, ".txt");

        for (int i = 0; i < 10; i++)
        {
            total_gacha++;
            check_primo(&primogems);
            gacha_haram(total_gacha, temp_dir);
        }
        sleep(1);
        srand(time(NULL));
    }
}
```
- `char current_dir[100]` untuk membuat format direktori sesuai ketentuan `total_gacha_{jumlah-gacha}`.
- `sprintf(num_dir, "%d", round_to_90)` memformat int `round_to_90` kedalam string lalu disimpan didalam variable `num_dir`.
- `for (int j = 0; j < 9; j++)` untuk looping per 1 folder (kelipatan 90).
- `time_t timer` untuk menggunakan fungsi `localtime()` kedepannya.
- `timer = time(NULL)` inisialisasi timer.
- `struct tm *tm` struct untuk memformat time menjadi struct
- `strftime(formatted_time, 100, "%H:%M:%S", tm_info)` memformat `datetime` sesuai format soal dan menyimpannya kedalam `formatted_time`.
- `for (int i = 0; i < 10; i++)` untuk looping per 10 gacha yang akan dimasukkan kedalam 1 file .txt.
- `srand(time(NULL))` untuk mereset `rand()` (didalam function `get_rand()`).

> Berikut adalah contoh file json dari database

![soal1_2.png](images/soal1_2.png)

Untuk kode dari function `check_primo()` adalah sebagai berikut.
```c
void check_primo(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 160)
    {
        time_t t;
        t = time(NULL);
        struct tm *tm = localtime(&t);
        char formatted_time[100];
        // bulan tgl jam menit, 30 Maret jam 07:44.
        // 3 30 07 44
        strftime(formatted_time, 100, "%m %d %H %M", tm);
        puts(formatted_time);
        while (strcmp(formatted_time, "03 30 07 44"))
        {
            wait(NULL);
        }
        zip_folder();
        delete_dir("weapons");
        delete_dir("characters");
        exit(EXIT_SUCCESS);
    }
}
```
Fungsi ini akan dijalankan setiap kali fungsi `gacha_haram()` akan dipanggil. Fungsi ini akan mengurangi `primogems` hingga dibawah batas gacha yaitu 160. <br>
Jika nilai `primogems` dibawah 160, maka program akan menunggu hingga waktu yang ditentukan. Penjelasan lebih lanjut akan diberikan di poin 1e.

Untuk kode dari function `gacha_haram()` adalah sebagai berikut.
```c
void gacha_haram(int count, char current_dir[100])
{

    int max;
    char path[100];
    DIR *dp;
    struct dirent *ep;
    char rand_path[100];
    char item_type[100];

    if (count % 2 == 0)
    {
        max = 130;
        strcpy(item_type, "Weapons");
        strcpy(path, "weapons/");
    }
    else
    {
        max = 48;
        strcpy(item_type, "Characters");
        strcpy(path, "characters/");
    }
    dp = opendir(path);

    int random = get_rand(max);
    if (dp != NULL)
    {
        for (int j = 0; j <= random; j++)
        {
            ep = readdir(dp);
        }
        strcpy(rand_path, ep->d_name);
        ep = NULL;
        closedir(dp);
    }
    else
    {
        perror("Couldn't open the directory");
    }
    // Baca json
    strcat(path, rand_path);
    char buffer[4096];
    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;
    FILE *fp;

    fp = fopen(path, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    FILE *gacha_file;
    char insert_into_file[100];
    char num[5], primo[10];
    sprintf(num, "%d", count);
    sprintf(primo, "%d", primogems);

    gacha_file = fopen(current_dir, "a");

    strcpy(insert_into_file, num);
    strcat(insert_into_file, "_");
    strcat(insert_into_file, item_type);
    strcat(insert_into_file, "_");
    strcat(insert_into_file, json_object_get_string(rarity));
    strcat(insert_into_file, "_");
    strcat(insert_into_file, json_object_get_string(name));
    strcat(insert_into_file, "_");
    strcat(insert_into_file, primo);
    strcat(insert_into_file, "\n");

    fputs(insert_into_file, gacha_file);
    fclose(gacha_file);
}
```
Dengan parameter `count` berupa total gacha yang sudah diakukan dan `current_dir[100]` untuk path ke file yang diinginkan.
- Variable `max`, `item_type`, dan `path` merupakan jumlah file yang ada di folder `weapons/` ataupun `characters/`. Nilai kedua variable tersebut akan diisi bergantung dari variable `count`. Jika `count` adalah genap, maka set dengan ketentuan dari `Weapons`. Dan jika genap, isi dengan ketentuan dari `Characters`.
- `max` adalah jumlah dari file pada path tertentu (untuk `weapons/` adalah 130, dan untuk `characters/` adalah 48). Ditentukan secara manual.
- `DIR *dp`, `struct dirent *dp`, `dp = opendir(path)` digunakan untuk directory listing.
- Terdapat `struct json_object` untuk menampung hasil json dari file .json yang diambil. Terdapat juga beberapa fungsi header `<json-c/json.h>` seperti mengambil hasil, mem-parse string json, dan lainnya.
- Pada bagian terakhir dari kode berfungsi untuk menuliskan hasil gacha sesuai format `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}` kedalam file dengan path berdasarkan variable `current_dir`

Penamaan path untuk folder dan file terdapat pada `int main()` yang nantinya akan dipassing kedalam fungsi `gacha_haram()`. <br>
Untuk kode penamaan folder/file adalah sebagai berikut.
```c
while (1)
{
    // create dir name
    char current_dir[100], num_dir[5];
    strcpy(current_dir, "gacha_gacha/");
    int round_to_90 = total_gacha - (total_gacha % 90);
    sprintf(num_dir, "%d", round_to_90);
    strcat(current_dir, "total_gacha_");
    strcat(current_dir, num_dir);
    strcat(current_dir, "/");
    create_dir(current_dir);

    for (int j = 0; j < 9; j++)
    {
        // create filename
        time_t timer;
        char formatted_time[100], number[5];
        char temp_dir[100];
        strcpy(temp_dir, current_dir);
        struct tm *tm_info;
        timer = time(NULL);
        tm_info = localtime(&timer);
        strftime(formatted_time, 100, "%H:%M:%S", tm_info);
        int round_to_10 = total_gacha - (total_gacha % 10);
        sprintf(number, "%d", round_to_10);

        strcat(temp_dir, formatted_time);
        strcat(temp_dir, "_gacha_");
        strcat(temp_dir, number);
        strcat(temp_dir, ".txt");

        // ...
    }
}
```
Sesuai letak comment, blok kode penamaan folder/file terletak di bawah comment tersebut.

> Berikut adalah contoh list directory dari soal

![soal1_3.png](images/soal1_3.png)

### 1e

Permintaan soal adalah membuat program berjalan secara daemon menggunakan template dari [modul](https://github.com/arsitektur-jaringan-komputer/Modul-Sisop/tree/master/Modul2#3-implementasi-daemon). <br>

> Berikut adalah bukti program berjalan di background

![ss_3a.png](images/soal1_1.png)

--- 
> Bagian Revisi

Permintaan lain dari soal adalah script akan jalan ketika `30 Maret jam 04:44`. Dan proses zip folder akan dilakukan 3 jam setelah program dijalankan. <br>
```c
void wait_date()
{
    time_t t;
    t = time(NULL);
    struct tm *tm = localtime(&t);
    char formatted_time[100];
    // bulan tgl jam menit, 30 Maret jam 04:44.
    // 3 30 04 44
    strftime(formatted_time, 100, "%m %d %H %M", tm);
    while (1)
    {
        if (strcmp(formatted_time, "03 30 04 44") == 0)
        {
            break;
        }
        wait(NULL);
        sleep(1);

        time_t t;
        t = time(NULL);
        struct tm *tm = localtime(&t);
        strftime(formatted_time, 100, "%m %d %H %M", tm);
    }
}

//...

void check_primo(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 160)
    {
        time_t t;
        t = time(NULL);
        struct tm *tm = localtime(&t);
        char formatted_time[100];
        // bulan tgl jam menit, 30 Maret jam 07:44.
        // 3 30 07 44
        strftime(formatted_time, 100, "%m %d %H %M", tm);

        while (1)
        {
            if (strcmp(formatted_time, "03 30 07 44") == 0)
                break;
            wait(NULL);
            sleep(1);
            time_t t;
            t = time(NULL);
            struct tm *tm = localtime(&t);
            strftime(formatted_time, 100, "%m %d %H %M", tm);
        }
        //...
    }
}
```
Fungsi `wait_date()` adalah untuk membuat program menunggu hingga waktu yang ditentukan, yaitu `30 Maret jam 04:44`. <br>
Sedangkan cuplikan kode pada fungsi `cek_primo()` membuat program menunggu hingga 3 jam setelah program dimulai.

Pada akhir program, semua folder dan file dalam folder `gacha_gacha/` akan di-zip dan dipassword sesuai ketentuan soal. <br>
Untuk kode zip adalah sebagai berikut.
```c
void zip_folder()
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        chdir("/home/kali/sisop/modul-2/Soal 1/gacha_gacha/");
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", ".", "-i", "*", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
```

> Berikut adalah hasil setelah di-zip

![soal1_4.png](images/soal1_4.png)

---

## Soal 2
Japrun bekerja di sebuah perusahaan dibidang review industri perfilman, karena kondisi saat
ini sedang pandemi Covid-19, dia mendapatkan sebuah proyek untuk mencari drama korea
yang tayang dan sedang ramai di Layanan Streaming Film untuk diberi review. Japrun sudah
mendapatkan beberapa foto-foto poster serial dalam bentuk zip untuk diberikan review,
tetapi didalam zip tersebut banyak sekali poster drama korea dan dia harus memisahkan
poster-poster drama korea tersebut tergantung dengan kategorinya. Japrun merasa kesulitan
untuk melakukan pekerjaannya secara manual, kamu sebagai programmer diminta Japrun
untuk menyelesaikan pekerjaannya.

## Penyelesaian soal 2
   - Membuat fungsi `process()` untuk menjalankan program yang nantinya akan melakukan fork dan execv berdasarkan *argv[] dan run[] agar fork yang dilakukan tidak mengganggu proses yang sedang berjalan.
   ```c
void process(char run[], char *argv[]){
    int status;
    pid_t child = fork();

    if(child == 0){
        execv(run, argv);
    } else while(wait(&status) > 0);
}
   ```

### 2a
- `Nomor 2a` mengextract zip yang diberikan ke dalam folder `/home/[user]/shift2/drakor`. Program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.
    - Saat program berhasil dijalankan, maka akan langsung melakukan unzip dimana pada unzip tersebut akan langsung memfilter file yang tidak dibutuhkan 
```c
    int main(){
    int status;
    pid_t child = fork();

    if(child == 0){
        char *argv[] = {"unzip", "drakor.zip", "*.png", "-d", "drakor", NULL};
        execv("/bin/unzip", argv);
    } else{
        while(wait(&status) > 0);
        processAll();

        return(0);
    }
}
```
Di dalam `char *argv[]` terdapat `*.png` yang mana nantinya pada proses unzip akan mengeluarkan file yang memiliki akhiran ".png". Kemudian terdapat `-d` dan `drakor` yang memproses setiap file yang diunzip dan akan dimasukan ke dalam folder drakor. 

    > Berikut adalah output dari jawaban 2a :

![ss_2a.jJPG](images/ss_2a.JPG)

### 2b dan 2c
- `Nomor 2b` poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus
membuat folder untuk setiap jenis drama korea yang ada dalam zip.
- `Nomor 2c` setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan direname dengan nama.
    - Setelah tadi berhasil diunzip, dilanjutkan ke fungsi `processAll()` yang akan membuka directory drakor yang dibuat pada fungsi `createDir()` untuk menelusuri semua gambar yang berisi poster-poster drakor
```c
void processAll(){
    DIR *d = opendir("drakor");
    struct dirent *dir;

    char ls[50][100];
    int i=0;

    if (d) {
        while ((dir = readdir(d)) != NULL) {
            if (strcmp(dir->d_name, ".") == 0 || strcmp(dir->d_name, "..") == 0 || strcmp(dir->d_name, "..") == 0) continue;
            char object[225];
            snprintf(object, sizeof object, "%s", dir->d_name);
            
            char str[225];
            strcpy(str, object);
            char *format = strtok(str, ";");

            char drakor[100];
            snprintf(drakor, sizeof drakor, "%s", format);
            format = strtok(NULL, ";");
           
            char year[50];
            snprintf(year, sizeof year, "%s", format);
            format = strtok(NULL, ";._");

            char genre[50];
            snprintf(genre, sizeof genre, "%s", format);
            format = strtok(NULL, ";");

            createDir(genre);

            copyAll(drakor, genre, object);
            
            snprintf(ls[i], sizeof ls[i], "%s;%s;%s", genre, year, drakor);
            i++;
```
 - Dengan menggunakan `strtok` dan delimiter `.;_` untuk mendapatkan nama, tahun rilis, serta kategori dari poster tersebut. 
 - Setelah didapatkan informasi dari poster tersebut, selanjutnya akan membuat folder berdasarkan kategori, dan menyalin file dengan mengubah nama berdasarkan nama poster drakor serta mencatat data.txt dengan tahun rilis. 

 > Berikut adalah output dari jawaban 2b :

![ss_2b.jJPG](images/ss_2b.JPG)

 > Berikut adalah output dari jawaban 2c :

![ss_2c.jJPG](images/ss_2c.JPG)

### 2d
- `Nomor 2d` karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus dipindah ke masing-masing kategori yang sesuai.
    - Dalam kasus 2 poster dalam satu file dengan mengecek hasil `strtok` setelah strtok ketiga kali. Jika hasil `strtok` tersebut memiliki panjang string yang lebih dari 3, maka file tersebut memiliki 2 poster dan akan dipindahkan ke masing-masing folder kategori yang sesuai
```c
    if(strlen(format) > 3){
                char drakor2[100];
                snprintf(drakor2, sizeof drakor2, "%s", format);
                format = strtok(NULL, ";");

                char year2[50];
                snprintf(year2, sizeof year2, "%s", format);
                format = strtok(NULL, ";._");
        
                char genre2[50];
                snprintf(genre2, sizeof genre2, "%s", format);
                format = strtok(NULL, "_");
  
                createDir(genre2);
                copyAll(drakor2, genre2, object);

                snprintf(ls[i], sizeof ls[i], "%s;%s;%s", genre2, year2, drakor2);
                i++;
            }
            deleteFile(object);
        }
        closedir(d);
    }
```
- Setelah file disalin, selanjutnya membuat list string `ls` yang nantinya akan menyimpan data kategori, tahun rilis, dan nama dengan format kategori;tahun;nama yang mana dengan format seperti ini akan membantu dalam mengurutkan berdasarkan tahun rilis untuk menulis data.txt.

 > Berikut adalah output dari jawaban 2d : 

![ss_2d1.jJPG](images/ss_2d1.JPG)

![ss_2d2.jJPG](images/ss_2d2.JPG)

Bisa dilihat pada gambar di atas terdapat 2 poster dalam 1 foto yang sudah dipisahkan sesuai kategorinya masing-masing, sebagai contoh yaitu drakor who are you dan love alarm yang berada dalam 1 poster foto dan sudah dipisahkan berdasarkan genrenya yaitu romance dan school

### 2e
- `Nomor 2e` di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama
dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).
    - Pada bagian ini khusus untuk menulis data.txt untuk setiap kategori. 
    - Untuk proses sorting list menggunakan Bubble Sort Algorithm yaitu metode pengurutan algoritma dengan cara melakukan penukaran data secara terus menerus sampai bisa dipastikan dalam suatu iterasi tertentu tidak ada lagi perubahan/penukaran
```c
    int it,jt;
    char temp[105];
    //Buble Sort algorithm
    for(it=0; it<20; it++){
        for(jt=0; jt<20-1-it; jt++){
            if(strcmp(ls[jt], ls[jt+1]) > 0){
                //swap ls[j] and ls[j+1]
                strcpy(temp, ls[jt]);
                strcpy(ls[jt], ls[jt+1]);
                strcpy(ls[jt+1], temp);
            }
        }
    }
    strcpy(temp, "");

    for(it=0; it<20; it++) printf("%i - %s\n", it, ls[it]);

    for(it=1; it<20; it++){
        //printf("%s\n", ls[it]);
        char st[2255];
        snprintf(st, sizeof st, "%s", ls[it]);
        //printf("%s\n", st);

        char *format = strtok(st, ";");
        char genre[15];
        snprintf(genre, sizeof genre, "%s", format);
        format = strtok(NULL, ";");

        //printf("get genre %s\n", genre);

        char year[15];
        snprintf(year, sizeof year, "%s", format);
        format = strtok(NULL, ";");

        //printf("get year %s\n", year);

        char drakor[100];
        snprintf(drakor, sizeof drakor, "%s", format);
        format = strtok(NULL, ";");

        //printf("get drakor %s\n", drakor);

        char path[50];
        snprintf(path, sizeof path, "drakor/%s/data.txt", genre);
        
        FILE *data;
        data = fopen(path, "a+");
        
        if(strcmp(temp, genre) != 0){
            
            fprintf(data, "genre: %s\n\n", genre);
        }
        fprintf(data, "nama: %s\n", drakor);
        fprintf(data, "rilis: tahun %s\n\n", year);
       
        fclose(data);
        strcpy(temp, genre);
    }
}
```
- Dengan memperhatikan kategori terlebih dahulu, saat mencapai awal dari kategori akan dibuatkan `kategori: nama kategori`
- Dengan menggunakan strtok dengan delimiter `;` bisa didapatkan kembali kategori, tahun rilis, dan nama drakor sehingga kita bisa menulis nama drakor dan tahun rilis di dalam `data.txt` sesuai kategori dari setiap drakor dan akan diurutkan di setiap kategori.

> Berikut adalah isi dari data.txt :

![ss_2e.jJPG](images/ss_2e.JPG)

### Penjelasan fungsi tambahan
1. Fungsi `createDir(char genre[])`
```c
void createDir(char genre[]){
    char path[225];
    snprintf(path, sizeof path, "drakor/%s", genre);
    char *argv[ ] = {"mkdir", "-p", path, NULL};

    process("/usr/bin/mkdir", argv);
}
```
- Fungsi ini akan membuat directory berdasarkan string yang diberikan yaitu genre[]. 

2. Fungsi `void copyAll(char name[], char genre[], char fileName[]) `
```c
void copyAll(char name[], char genre[], char fileName[]) {
    char path[225];
    char newFile[225];
    snprintf(path, sizeof path, "drakor/%s", fileName);
    snprintf(newFile, sizeof newFile, "drakor/%s/%s.png", genre, name);
    
    char *argv[] = {"cp", path, newFile, NULL};
    char run[] = "/usr/bin/cp";
    process(run, argv);
}
```
- Fungsi ini akan menyalin file berdasarkan fileName[] ke dalam genre[] dengan nama name[].

3. Fungsi `void deleteFile(char fileName[])`
```c
void deleteFile(char fileName[]){
    char path[225];
    snprintf(path, sizeof path, "drakor/%s", fileName);

    char *argv[ ] = {"rm", path, NULL};
    char run[ ] = "/usr/bin/rm";
    process(run, argv);
}
```
- Fungsi ini akan menghapus file berdasarkan fileName[]

#### Output jika program berhasil dijalankan
![ss2.jJPG](images/ss2.JPG)

### Kendala soal 2
Kami sering mendapati segmentation error. Hal ini terjadi karena kami mengunjungi directory yang masih belum dibuat. Kemudian saat melakukan strtok, karena delimiter yang diberikan kurang maka terdapat directory dengan nama (null).

---

## Soal 3
Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang dengan mengerjakan task berikut.

## Penyelesaian soal 3
   - Dalam program ini dibuat function `child_p()` yaitu function untuk membuat child process. Function ini dibuat karena proses pembuatan child dilakukan lebih dari tiga kali sehingga diharapkan dapat menyederhanakan code. Berikut adalah isi function tersebut:
   ```c
    void child_p (pid_t *cid, char *arg, char **argv) {
        *cid = fork();
        if (*cid < 0) exit(1);
        if (*cid == 0) {
            execv(arg, argv);
            exit(0);
        }
    return;
    }
   ```

### 3a
- `Nomor 3a` membuat 2 directory di “/home/najwamel/modul2/” dengan nama “darat”, 3 detik kemudian membuat directory ke 2 dengan nama “air”.
    - Pada code di bawah ini dengan konsep `wait x fork x exec`, pertama program akan membuat directory "/home/najwamel/modul2/darat" lalu diberikan delay 3 detik dengan menggunakan `sleep(3)` sebelum membuat directory kedua "/home/najwamel/modul2/air" 
```c
    cid1 = fork ();
    if (cid1 < 0) exit(EXIT_FAILURE);
    if (cid1 == 0) {
      char *argv1[] = {"mkdir", "-p", "modul2/darat", NULL};
      execv("/usr/bin/mkdir", argv1);
    } else if (cid > 0) while(wait(&status) > 0);
    
    // membuat folder air di modul2
    cid2 = fork();
    if(cid2 < 0) exit(EXIT_FAILURE);
    if(cid2 == 0){
      sleep(3);
      char *argv2[] = {"mkdir", "-p", "modul2/air", NULL};
      execv("/bin/mkdir", argv2);
      while(wait(&status) > 0);
    }
```
    > Berikut adalah screenshot hasil dari jawaban 3a:

![ss_3a.png](images/ss_3a.png)

### 3b
- `Nomor 3b` meng-extract “animal.zip” di “/home/najwamel/modul2/”.
    - Pada code di bawah ini, file animal.zip yang sudah ditempatkan pada directory “/home/najwamel/animal.zip” disimpan ke dalam variabel `zip_file[]` kemudian dilakukan ekstraksi dengan method unzip ke dalam folder modul2 dan membuat child processnya dengan memanggil fungsi `child_p` 
```c
    char *unzip[] = {"/bin/unzip", "unzip"};
    char zip_file[]= "/home/najwamel/animal.zip";
    char *argv3[] = {unzip[1], zip_file, "-d", "modul2", "*/*", NULL};
    child_p (&cid3, unzip[0], argv3);
    while(wait(&status) > 0);
```

### 3c
- `Nomor 3c` memisah hasil ekstrak menjadi hewan darat ke “/home/najwamel/modul2/darat” dan hewan air ke “/home/najwamel/modul2/air”, serta menghapus hewan yang tidak ada keterangan air atau darat.
    - Pada code di bawah ini, pertama akan memisah hewan darat ke folder darat dengan method `find` untuk menemukan keyword `darat.jpg` dan `bird.jpg` kemudian disalin ke direktori "/home/najwamel/modul2/darat" dengan command `cp` dan membuat child processnya dengan memanggil fungsi `child_p` 
```c
    char *find[] = {"/bin/find", "find"};
    char *argv4[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*darat.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/darat", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid4, find[0], argv4);
    while(wait(&status) > 0);
    
    char *argv5[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*bird.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/darat", 
      	   	   "{}", "+", (char *) NULL};
    child_p (&cid5, find[0], argv5);
    while(wait(&status) > 0);
```
- Selanjutnya code dibawah ini adalah untuk memisah hewan air dari folder animal ke folder air dengan konsep yang sama seperti yang sebelumnya, hanya saja pada keywordnya yang digunakan kali ini adalah `air.jpg` dan `fish.jpg` kemudian disalin ke direktori "/home/najwamel/modul2/air" dan membuat child processnya dengan memanggil fungsi `child_p` 
```c
    char *argv6[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*air.jpg",
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/air", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid6, find[0], argv6);
    while(wait(&status) > 0);
    
    char *argv7[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*fish.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/air", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid7, find[0], argv7);
    while(wait(&status) > 0);
```
- Kemudian code di bawah ini untuk menghapus hewan yang tidak ada keterangan air atau darat yaitu `frog.jpg` dengan menggunakan command `rm` dan membuat child processnya dengan memanggil fungsi `child_p` 
```c
    char *argv8[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*frog.jpg", 
      		   "-exec", "rm", "-r", "{}", "+", (char *) NULL};
    child_p (&cid8, find[0], argv8);
    while(wait(&status) > 0);
```

### 3d
- `Nomor 3d` menghapus burung pada “/home/najwamel/modul2/darat”
    - Pada code di bawah ini akan menghapus semua burung dengan method `find` untuk menemukan keyword `bird.jpg` dan `bird_darat.jpg` di direktori "/home/najwamel/modul2/darat" dengan command `rm` dan membuat child processnya dengan memanggil fungsi `child_p` 
```c
    char *argv9[]={find[1], "/home/najwamel/modul2/darat", 
      		   "-type", "f", 
      		   "-name", "*bird.jpg", 
      		   "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
    child_p (&cid9, find[0], argv9);
    while(wait(&status) > 0);
    
    char *argv10[]={find[1], "/home/najwamel/modul2/darat", 
      		   "-type", "f", 
      		   "-name", "*bird_darat.jpg", 
      		   "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
    child_p (&cid10, find[0], argv10);
    while(wait(&status) > 0);
```

### 3e
- `Nomor 3e` membuat file list.txt di folder “/home/najwamel/modul2/air” berisi list nama semua hewan yang ada di direktori tersebut dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
    - Pada code di bawah ini akan file `list.txt` dalam folder air dengan command `touch` dan membuat child processnya dengan memanggil fungsi `child_p` kemudian memanggil fungsi `buat_list` untuk menampilkan outputnya dalam file list.txt
```c
    char *touch[] = {"/bin/touch", "touch"};
    char *argv11[] = {touch[1], "/home/najwamel/modul2/air/list.txt", NULL};
    child_p (&cid11, touch[0], argv11);
    while(wait(&status) > 0);
```
- Code di bawah ini merupakan fungsi `buat_list` yang akan menampilakan output berupa list nama semua hewan yang ada di folder air dengan format `UID_[UID file permission]_Nama File.[jpg/png]` ke dalam file `list.txt` dimana untuk mendapatkan UID didapat dari method `getlogin()`
    - Kemudian membuka file list.txt pada modul2/air. Lalu jika ketika string dibandingkan menggunakan strcmp tidak sama dengan 0, dan teridentifikasi sebagai file jpg, maka akan dieksekusi untuk mengecek file permissionnya.
    - File permission tersimpan pada `st_mode` member, sehingga untuk S_IRUSR maka file permissionnya `r` atau read, jika S_IWUSR berupa `w` atau write, dan untuk S_IXUSR adalah `x` atau execute.
    - Terakhir dengan `fprint` untuk menampilkan output sesuai format yaitu IUD_fp_namafile.jpg ke dalam file list.txt 
```c
void buat_list(){
    char path[1000] = "/home/najwamel/modul2/air";
    struct dirent *dp;
    DIR *dir = opendir(path);
    struct stat fs;
    char *UID;
    int r;
    UID = (char *)malloc(10*sizeof(char));
    UID = getlogin();
    FILE *list_txt = fopen("/home/najwamel/modul2/air/list.txt", "w");
    if(dir!=NULL) {
	  while((dp = readdir(dir))) {
	    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, "jpg")) {	
		char namafile[100] = "modul2/air/";
		r = stat(namafile, &fs);
		strcat(namafile, dp->d_name);
		char fp1, fp2, fp3 = ' ';
	 	if( fs.st_mode & S_IRUSR )fp1 = 'r';
		if( fs.st_mode & S_IWUSR )fp2 = 'w';
		if( fs.st_mode & S_IXUSR )fp3 = 'x';
		fprintf(list_txt, "%s_%c%c%c_%s\n", UID, fp1, fp2, fp3, dp->d_name);
	    }
	  } 
	  closedir(dir);
    }
    fclose(list_txt);
}
```
    > Berikut adalah screenshot hasil soal nomer 3b-3e:
- `Gambar 1` : output setelah meng-ekstrak animal.zip dan penghapusan hewan tanpa keterangan air/darat di folder animal
![ss_3bc.png](images/ss_3bc.png)

- `Gambar 2` : output setelah pemisahan hewan darat di folder darat dan penghapusan semua burung
![ss_3cd.png](images/ss_3cd.png)

- `Gambar 3` : output setelah pemisahan hewan air di folder air dan pembuatan file list.txt
![ss_3ce.png](images/ss_3ce.png)

- `Gambar 4` : output isi dari file list.txt
![ss_3e.png](images/ss_3e.png)

### Kendala soal 3
Kendala yang dihadapi dalam pengerjaan soal3 yaitu pada poin 3e dimana awalnya output yang keluar dalam file list.txt tidak sesuai format yang diminta, namun setelah meng-concat setiap string yang diminta dengan urutan yang benar list yang keluar dapat sesuai formatnya.

