#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>
#include <time.h>
#include <json-c/json.h>
#include <dirent.h>

int primogems = 79000;
char *url_chara = "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download";
char *url_weap = "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download";

void download(char *url, char *filename);
void unzip(char *filename);
void create_dir(char *dirname);
void gacha_haram(int count, char current_dir[100]);
int get_rand(int max);
void check_primo(int *primo);
void delete_dir(char dir[100]);
void zip_folder();
void wait_date();

int main()
{
    pid_t pid, sid; // Variabel untuk menyimpan PID

    pid = fork(); // Menyimpan PID dari Child Process

    /* Keluar saat fork gagal
     * (nilai variabel pid < 0) */
    if (pid < 0)
    {
        exit(EXIT_FAILURE);
    }

    /* Keluar saat fork berhasil
     * (nilai variabel pid adalah PID dari child process) */
    if (pid > 0)
    {
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();
    if (sid < 0)
    {
        exit(EXIT_FAILURE);
    }

    if ((chdir("/home/kali/sisop/modul-2/Soal 1")) < 0)
    {
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    wait_date();
    download(url_chara, "characters.zip");
    download(url_weap, "weapons.zip");
    unzip("characters.zip");
    unzip("weapons.zip");
    create_dir("gacha_gacha");
    int total_gacha = 0;
    while (1)
    {
        // create dir name
        char current_dir[100], num_dir[5];
        strcpy(current_dir, "gacha_gacha/");
        int round_to_90 = total_gacha - (total_gacha % 90);
        sprintf(num_dir, "%d", round_to_90);
        strcat(current_dir, "total_gacha_");
        strcat(current_dir, num_dir);
        strcat(current_dir, "/");
        create_dir(current_dir);

        for (int j = 0; j < 9; j++)
        {
            // create filename
            time_t timer;
            char formatted_time[100], number[5];
            char temp_dir[100];
            strcpy(temp_dir, current_dir);
            struct tm *tm_info;
            timer = time(NULL);
            tm_info = localtime(&timer);
            strftime(formatted_time, 100, "%H:%M:%S", tm_info);
            int round_to_10 = total_gacha - (total_gacha % 10);
            sprintf(number, "%d", round_to_10);

            strcat(temp_dir, formatted_time);
            strcat(temp_dir, "_gacha_");
            strcat(temp_dir, number);
            strcat(temp_dir, ".txt");

            for (int i = 0; i < 10; i++)
            {
                total_gacha++;
                check_primo(&primogems);
                gacha_haram(total_gacha, temp_dir);
            }
            sleep(1);
            srand(time(NULL));
        }
    }
}

void wait_date()
{
    time_t t;
    t = time(NULL);
    struct tm *tm = localtime(&t);
    char formatted_time[100];
    // bulan tgl jam menit, 30 Maret jam 04:44.
    // 3 30 04 44
    strftime(formatted_time, 100, "%m %d %H %M", tm);
    while (1)
    {
        if (strcmp(formatted_time, "03 30 04 44") == 0)
        {
            break;
        }
        wait(NULL);
        sleep(1);

        time_t t;
        t = time(NULL);
        struct tm *tm = localtime(&t);
        strftime(formatted_time, 100, "%m %d %H %M", tm);
    }
}

void download(char *url, char *filename)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"wget", "-q", "--no-check-certificate", url, "-O", filename, NULL};
        execv("/bin/wget", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
void unzip(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"unzip", "-n", "-q", filename, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
void create_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        DIR *dir = opendir(dirname);
        if (dir)
        {
            closedir(dir);
        }
        else
        {
            char *argv[] = {"mkdir", dirname, NULL};
            execv("/bin/mkdir", argv);
        }
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
int get_rand(int max)
{
    return (rand() % max);
}
void check_primo(int *primo)
{
    *primo = *primo - 160;
    if (*primo <= 160)
    {
        time_t t;
        t = time(NULL);
        struct tm *tm = localtime(&t);
        char formatted_time[100];
        // bulan tgl jam menit, 30 Maret jam 07:44.
        // 3 30 07 44
        strftime(formatted_time, 100, "%m %d %H %M", tm);

        while (1)
        {
            if (strcmp(formatted_time, "03 30 07 44") == 0)
                break;
            wait(NULL);
            sleep(1);
            time_t t;
            t = time(NULL);
            struct tm *tm = localtime(&t);
            strftime(formatted_time, 100, "%m %d %H %M", tm);
        }
        zip_folder();
        delete_dir("weapons");
        delete_dir("characters");
        exit(EXIT_SUCCESS);
    }
}

void delete_dir(char dir[100])
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        char *argv[] = {"rm", "-rf", dir, NULL};
        execv("/bin/rm", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}
void zip_folder()
{
    pid_t child_id;
    child_id = fork();

    if (child_id == 0)
    {
        chdir("/home/kali/sisop/modul-2/Soal 1/gacha_gacha/");
        char *argv[] = {"zip", "-q", "-m", "-r", "-P", "satuduatiga", "not_safe_for_wibu.zip", ".", "-i", "*", NULL};
        execv("/bin/zip", argv);
        exit(EXIT_SUCCESS);
    }
    else if (child_id > 0)
    {
        wait(NULL);
    }
}

void gacha_haram(int count, char current_dir[100])
{
    int max;
    char path[100];
    DIR *dp;
    struct dirent *ep;
    char rand_path[100];
    char item_type[100];

    if (count % 2 == 0)
    {
        max = 130;
        strcpy(item_type, "Weapons");
        strcpy(path, "weapons/");
    }
    else
    {
        max = 48;
        strcpy(item_type, "Characters");
        strcpy(path, "characters/");
    }
    dp = opendir(path);

    int random = get_rand(max);
    if (dp != NULL)
    {
        for (int j = 0; j <= random; j++)
        {
            ep = readdir(dp);
        }
        strcpy(rand_path, ep->d_name);
        ep = NULL;
        closedir(dp);
    }
    else
    {
        perror("Couldn't open the directory");
    }
    // Baca json
    strcat(path, rand_path);
    char buffer[4096];
    struct json_object *parsed_json;
    struct json_object *rarity;
    struct json_object *name;
    FILE *fp;

    fp = fopen(path, "r");
    fread(buffer, 4096, 1, fp);
    fclose(fp);

    parsed_json = json_tokener_parse(buffer);
    json_object_object_get_ex(parsed_json, "name", &name);
    json_object_object_get_ex(parsed_json, "rarity", &rarity);

    FILE *gacha_file;
    char insert_into_file[100];
    char num[5], primo[10];
    sprintf(num, "%d", count);
    sprintf(primo, "%d", primogems);

    gacha_file = fopen(current_dir, "a");

    strcpy(insert_into_file, num);
    strcat(insert_into_file, "_");
    strcat(insert_into_file, item_type);
    strcat(insert_into_file, "_");
    strcat(insert_into_file, json_object_get_string(rarity));
    strcat(insert_into_file, "_");
    strcat(insert_into_file, json_object_get_string(name));
    strcat(insert_into_file, "_");
    strcat(insert_into_file, primo);
    strcat(insert_into_file, "\n");

    fputs(insert_into_file, gacha_file);
    fclose(gacha_file);
}
