#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <wait.h>
#include <dirent.h>
#include <time.h>
#include <fcntl.h>
#include <stdbool.h>

void child_p (pid_t *cid, char *arg, char **argv) {
    *cid = fork();
    if (*cid < 0) exit(1);
    if (*cid == 0) {
        execv(arg, argv);
        exit(0);
    }
    return;
}
void buat_list();

int main() {
    pid_t cid, cid1, cid2, cid3, cid4, cid5, cid6, cid7, cid8, cid9, cid10, cid11;
    int status;
  
    // 3a
    // membuat folder modul2
    cid = fork();
    if (cid < 0) exit(EXIT_FAILURE);
    if (cid == 0) {
      char *argv[] = {"mkdir", "-p", "modul2", NULL};
      execv("/bin/mkdir", argv);
      exit(EXIT_SUCCESS);
      while ((wait(&status)) > 0);
    } 
    
    // membuat folder darat di modul2
    cid1 = fork ();
    if (cid1 < 0) exit(EXIT_FAILURE);
    if (cid1 == 0) {
      char *argv1[] = {"mkdir", "-p", "modul2/darat", NULL};
      execv("/usr/bin/mkdir", argv1);
    } else if (cid > 0) while(wait(&status) > 0);
    
    // membuat folder air di modul2
    cid2 = fork();
    if(cid2 < 0) exit(EXIT_FAILURE);
    if(cid2 == 0){
      sleep(3);
      char *argv2[] = {"mkdir", "-p", "modul2/air", NULL};
      execv("/bin/mkdir", argv2);
      while(wait(&status) > 0);
    }
  
    // 3b  
    // meng-extract animal.zip ke modul2
    char *unzip[] = {"/bin/unzip", "unzip"};
    char zip_file[]= "/home/najwamel/animal.zip";
    char *argv3[] = {unzip[1], zip_file, "-d", "modul2", "*/*", NULL};
    child_p (&cid3, unzip[0], argv3);
    while(wait(&status) > 0);

    // 3c
    // memisah hewan darat dari folder animal ke darat
    char *find[] = {"/bin/find", "find"};
    char *argv4[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*darat.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/darat", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid4, find[0], argv4);
    while(wait(&status) > 0);
    
    char *argv5[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*bird.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/darat", 
      	   	   "{}", "+", (char *) NULL};
    child_p (&cid5, find[0], argv5);
    while(wait(&status) > 0);
    
    // memisah hewan air dari folder animal ke air
    char *argv6[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*air.jpg",
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/air", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid6, find[0], argv6);
    while(wait(&status) > 0);
    
    char *argv7[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*fish.jpg", 
      		   "-exec", "cp", "-t", "/home/najwamel/modul2/air", 
      		   "{}", "+", (char *) NULL};
    child_p (&cid7, find[0], argv7);
    while(wait(&status) > 0);
    
    // menghapus hewan yang tidak ada keterangan air atau darat
    char *argv8[]={find[1], "/home/najwamel/modul2/animal", 
      		   "-type", "f", 
      		   "-name", "*frog.jpg", 
      		   "-exec", "rm", "-r", "{}", "+", (char *) NULL};
    child_p (&cid8, find[0], argv8);
    while(wait(&status) > 0);
    
    // 3d
    // menghapus burung pada folder darat
    char *argv9[]={find[1], "/home/najwamel/modul2/darat", 
      		   "-type", "f", 
      		   "-name", "*bird.jpg", 
      		   "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
    child_p (&cid9, find[0], argv9);
    while(wait(&status) > 0);
    
    char *argv10[]={find[1], "/home/najwamel/modul2/darat", 
      		   "-type", "f", 
      		   "-name", "*bird_darat.jpg", 
      		   "-exec", "rm", "-r",  "{}", "+", (char *) NULL};
    child_p (&cid10, find[0], argv10);
    while(wait(&status) > 0);
    
    // 3e
    // membuat list.txt di folder air dengan format UID_[UID file permission]_Nama File.[jpg/png]
    char *touch[] = {"/bin/touch", "touch"};
    char *argv11[] = {touch[1], "/home/najwamel/modul2/air/list.txt", NULL};
    child_p (&cid11, touch[0], argv11);
    while(wait(&status) > 0);
    buat_list();
}

// fungsi untuk membuat list.txt di folder air dengan format UID_[UID file permission]_Nama File.[jpg/png]
void buat_list(){
    char path[1000] = "/home/najwamel/modul2/air";
    struct dirent *dp;
    DIR *dir = opendir(path);
	
    struct stat fs;
    char *UID;
    int r;
	
     UID = (char *)malloc(10*sizeof(char));
     UID = getlogin();
	
     FILE *list_txt = fopen("/home/najwamel/modul2/air/list.txt", "w");

     if(dir!=NULL) {
	while((dp = readdir(dir))) {
	    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strstr(dp->d_name, "jpg")) {	
		char namafile[100] = "modul2/air/";
		r = stat(namafile, &fs);
		strcat(namafile, dp->d_name);
		
		char fp1, fp2, fp3 = ' ';
	 	if( fs.st_mode & S_IRUSR )fp1 = 'r';
		if( fs.st_mode & S_IWUSR )fp2 = 'w';
		if( fs.st_mode & S_IXUSR )fp3 = 'x';
		
		fprintf(list_txt, "%s_%c%c%c_%s\n", UID, fp1, fp2, fp3, dp->d_name);
	    }
	} 
	closedir(dir);
    }
    fclose(list_txt);
}
